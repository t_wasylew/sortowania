package com.sda.sortin;

public class InsertionSort {
    public static void sort(int[] array) {
        int n = array.length;
        int counter = 0;
        // rozpoczynamy od "lewej" strony i bierzemy co obieg tej pętli (for)
        // o jeden więcej elementów do przeniesienia.
        for (int i = 1; i < n; i++) {
            int wartosc = array[i];// przepisuję sobie wartość nowo 'włączonego' elementu
            int j = i;  // przepisuje sobie indeks nowego elementu

            while (j > 0) { // dopóki są elementy z lewej strony
                counter++; // licznik instrukcji porównania
                if (wartosc < array[j - 1]) { // jeśli wartość jest mniejsza
                    array[j] = array[j - 1]; // przestawiam dany element w prawo [ żeby zrobić miejsce dla
                    // elementu który chce przestawić w lewo - 'wartość']
                } else {
                    break; // jeśli elementy z lewej strony są większe od zmiennej 'wartość'
                    // to znaczy że nie musimy przesuwać się bardziej w lewo.
                }
                j = j - 1; // dekrementacja licznika (przesuwam się w lewo)
            }
            array[j] = wartosc; // wstawiam wartość w znalezione miejsce (wszystkie elementy po lewej są posortowane)
//            BubbleSort.printArray(array); // wypisz
        }

        System.out.println("Licznik: " + counter); // wypisz licznik
    }
}
